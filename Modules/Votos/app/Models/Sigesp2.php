<?php

namespace Modules\Votos\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Votos\Database\factories\Sigesp2Factory;

class Sigesp2 extends Model
{
    use HasFactory;

    protected $connection = 'ficha';
    protected $table ='workerhp';
}
