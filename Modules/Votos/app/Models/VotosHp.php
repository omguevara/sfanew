<?php

namespace Modules\Votos\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Votos\Database\factories\VotosHpFactory;
use App\Traits\EncryptationId;

class VotosHp extends Model
{
    use HasFactory, EncryptationId;
    protected $table = "votoshp";
    public $timestamps = false;
    protected $appends = ['crypt_id'];

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'tipo_documento',
        'documento',
    ];

    protected $hidden = [
        'id',
        'fecha_registro'
    ];

    protected static function newFactory(): VotosFactory
    {
        //return VotosFactory::new();
    }
}
