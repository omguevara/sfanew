<?php

namespace Modules\Votos\app\Http\Controllers;
use \Modules\Votos\app\Http\Requests\VotosRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Modules\Votos\app\Models\Votos;
use \Modules\Votos\app\Models\VotosHp;
use \Modules\Votos\app\Models\Sigesp;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use DataTables;

class VotosController extends Controller
{

    protected $votos;
    protected $votoshp;
    protected $date;
    public function __construct() {
        $this->votos = new Votos();
        $this->votoshp = new votoshp();
        $this->date=Carbon::now();
    }

    /**
     * Display a listing of the resource.
     */
    public function ListUser()
    {
        return view('votos::index');
    }

    public function FormVotaciones(){
        //return view('votos::votaciones');
        return view('votos::formmenuvotos');
    }

    public function RegistroVoto(VotosRequest $request){
        $client = new Client();
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'document?documento='.base64_encode(json_encode($request->documento)).'&port='.base64_encode(json_encode(session('airport'))));
        $data=json_decode($response->getBody()->getContents());
        if(!empty($data->documento)){
            $this->votos->tipo_documento=$request->tipo_documento;
            $this->votos->documento=$request->documento;
            $this->votos->ip=$request->ip();
            $this->votos->user_id=Auth::id();
            $this->votos->save();
            return redirect()->route("votaciones")->with('success','Gracias por notificar su participacion en el CONSEJO NACIONAL ELECTORAL');
        }else{
            $client = new Client();
            $response = $client->get($baseUrl.'searchworkerhp?documento='.base64_encode(json_encode($request->documento)).'&port='.base64_encode(json_encode(session('airport'))));
            if(json_decode($response->getBody()->getContents())==1){
                $this->votoshp->tipo_documento=$request->tipo_documento;
                $this->votoshp->documento=$request->documento;
                $this->votoshp->ip=$request->ip();
                $this->votoshp->user_id=Auth::id();
                $this->votoshp->save();
                return redirect()->route("votaciones")->with('success','Gracias por notificar su participacion en el CONSEJO NACIONAL ELECTORAL');
            }
            return redirect()->route("votaciones")->with('errors','Esta intentando anunciar el voto de una persona que no trabaja en nuestras instalaciones.');
        }
    }

    public function ListVotantes(){//votos ejercidos
        $client = new Client();
        $all=$this->votos::all();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'jobi?votoe='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport'))));

        $data = Datatables::of(json_decode($response->getBody(), true))
                ->addIndexColumn()
                ->addColumn('foto', function ($row) {
                    return "<img style='width:80px; min-width:80px' loading='lazy'  src='http://127.0.0.1:8000/api/foto2/".$row['crypt_id']."' >";
                })
                ->addColumn('voto', function ($row) {
                    return "Voto Ejercido";
                })
                ->addColumn('botones', function ($row) {
                    $tip_document = $row["tipo_documento"];
                    $document = $row["document"];
                    return '<a onclick="Cne('."'". $tip_document . "'".','."'". $document . "'".')"><i class="fa fa-regular fa-clipboard fa-2x"></i></a>';
                })
                ->rawColumns(['foto','voto','botones'])
                ->make(true);
                $data1 = $data->original['data'];

        return $data1;
    }

    public function VotosNoEjercidos(){//votos no ejercidos
        $client = new Client();
        $all=$this->votos::all();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'jobn?voton='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport'))) );

        $data = Datatables::of(json_decode($response->getBody(), true))
                ->addIndexColumn()
                ->addColumn('foto', function ($row) {
                    return "<img style='width:80px; min-width:80px'  loading='lazy' src='http://127.0.0.1:8000/api/foto2/".$row['crypt_id']."'>";
                })
                ->addColumn('voto', function ($row) {
                    return "Voto No Ejercido";
                })
                ->addColumn('botones', function ($row) {
                    $tip_document = $row["tipo_documento"];
                    $document = $row["document"];
                    return '<a onclick="Cne('."'". $tip_document . "'".','."'". $document . "'".')"><i class="fa fa-regular fa-clipboard fa-2x"></i></a>';
                })
                ->rawColumns(['foto','voto','botones'])
                ->make(true);
                $data1 = $data->original['data'];

        return $data1;
    }

    public function CountVotos(){
        $baseUrl = env('API_ENDPOINT');
        $client = new Client();
        $all=$this->votos::all();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }

        $votose  = $client->get($baseUrl.'jobi?votoe='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport'))));//Votos E
        $votosne = $client->get($baseUrl.'jobn?voton='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport')))); ;//Votos No E
        //$cantairport = $client->get($baseUrl.'countairport?count='.base64_encode(json_encode($array)));//cant no ha votado
        //$cantvotoe = $client->get($baseUrl.'countvote?count='.base64_encode(json_encode($array)));//cant ejercieron el voto

        $votoejercido = $client->get($baseUrl.'listjobvotoejercido?vote='.base64_encode(json_encode($array)));//cant ejercieron el voto

        $air=[[],[]];
        $votne=[[],[]];
        $ChartGlobal=['oaci'=>[],'votose'=>[],'votosne'=>[]];
        // foreach(json_decode($cantairport->getBody()) as $airport){
        //     array_push($air[0],$airport->oaci);
        //     array_push($air[1],$airport->trabajadores);
        // }

        // foreach(json_decode($cantvotoe->getBody()) as $airport){
        //     array_push($votne[0],$airport->oaci);
        //     array_push($votne[1],$airport->trabajadores);
        // }

        foreach(json_decode($votoejercido->getBody()) as $data2){
            array_push($ChartGlobal['oaci'],$data2->oaci);
            array_push($ChartGlobal['votose'],$data2->votoejercido);
            array_push($ChartGlobal['votosne'],$data2->votoenojercido);
        }

        ///////////////////////////////////////////////////////////////////////////////
        //////////////////////////HP EJERCIDOS/////////////////////////////////////////
        $allhps=$this->votoshp::get();
        $arrayhp=[];
        foreach ($allhps as $keyhp) { array_push($arrayhp,$keyhp->documento); }
        $response2 = $client->get($baseUrl.'workerhpactive?votoe='.base64_encode(json_encode($arrayhp)).'&port='.base64_encode(json_encode(session('airport'))));
        //////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        //////////////////////////HP NO EJERCIDOS//////////////////////////////////////
        $hpne = $client->get($baseUrl.'workerhpinactive?votoe='.base64_encode(json_encode($arrayhp)).'&port='.base64_encode(json_encode(session('airport'))));;

        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        return [
            'votoe'=>count(json_decode($votose->getBody())),//votos ejercidos
            'votosne'=>count(json_decode($votosne->getBody())),//votos no ejercidos
            'votoshpe'=>count(json_decode($response2->getBody())),
            'votoshpne'=>count(json_decode($hpne->getBody())),
            'chartvotone'=>$ChartGlobal
            //'cantforairport'=>$air,//chart votos no ejercidos
            //'Chartvote'=>$votne,//char votos ejercidos
        ];
    }

    public function CountVotesByAirport(){
        $client = new Client();
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'airport?port='.base64_encode(json_encode(session('airport'))));
        return json_decode($response->getBody());
    }

    public function BuscarDatosAeropuerto(Request $request ){
        $all=$this->votos::all();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }

        $client = new Client();
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'SearchAirport?dat='.base64_encode(json_encode($request->dat)).'&j='.base64_encode(json_encode($array)));
        $d=json_decode($response->getBody(),true);
        $oficina=[];
        $si=[];
        $no=[];

        for ($i=0; $i < count($d); $i++) {
            array_push($oficina,$d[$i]['descripcion']);
            array_push($si,$d[$i]['si']);
            array_push($no,$d[$i]['no']);
        }
        return array(0=>$oficina,1=>$si,2=>$no);
    }

    public function IndexWorkerHp(){
        return view('votos::workerhp');
    }

    public function ListWorkerHpVote(){//hp ejercieron boto

        $client = new Client();
        $all=$this->votoshp::get();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }

        $baseUrl = env('API_ENDPOINT');

        $response = $client->get($baseUrl.'workerhpactive?votoe='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport'))));

        $data = Datatables::of(json_decode($response->getBody(), true))
                ->addIndexColumn()
                ->addColumn('foto', function ($row) {
                    $baseUrl = env('API_ENDPOINT');
                    //return "<img style='width:80px; min-width:80px' loading='lazy'  src='http://127.0.0.1:8000/api/foto/".$row['crypt_id']."' >";
                    $url=$baseUrl."foto/".$row['crypt_id'];

                    return "<img style='width:80px; min-width:80px' loading='lazy'  src='".$url."'>";
                })
                ->addColumn('voto', function ($row) {
                    return "Voto Ejercido";
                })
                ->addColumn('botones', function ($row) {
                    $tip_document = $row["tipo_documento"];
                    $document = $row["documento"];
                    return '<a onclick="Cne('."'". $tip_document . "'".','."'". $document . "'".')"><i class="fa fa-regular fa-clipboard fa-2x"></i></a>';
                })
                ->rawColumns(['foto','voto','botones'])
                ->make(true);
                $data1 = $data->original['data'];
        return $data1;
    }

    public function ListWorkerHpNoVote(){
        $client = new Client();
        $all=$this->votoshp::get();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'workerhpinactive?votoe='.base64_encode(json_encode($array)).'&port='.base64_encode(json_encode(session('airport'))));

        $data = Datatables::of(json_decode($response->getBody(), true))
                ->addIndexColumn()
                ->addColumn('foto', function ($row) {
                    //return "<img style='width:80px; min-width:80px' loading='lazy'  src='http://127.0.0.1:8000/api/foto/".$row['crypt_id']."' >";
                    $baseUrl = env('API_ENDPOINT');
                    $url=$baseUrl."foto/".$row['crypt_id'];
                    return "<img style='width:80px; min-width:80px' loading='lazy'  src='".$url."'>";
                })
                ->addColumn('voto', function ($row) {
                    return "Voto No Ejercido";
                })
                ->addColumn('botones', function ($row) {
                    $tip_document = $row["tipo_documento"];
                    $document = $row["documento"];
                    return '<a onclick="Cne('."'". $tip_document . "'".','."'". $document . "'".')"><i class="fa fa-regular fa-clipboard fa-2x"></i></a>';
                })
                ->rawColumns(['foto','voto','botones'])
                ->make(true);
                $data1 = $data->original['data'];
        return $data1;
    }

    public function GetWorkerHpByAirport(Request $request){
        $all=$this->votoshp::all();
        $array=[];
        foreach ($all as $key) { array_push($array,$key->documento); }
        $client = new Client();
        $baseUrl = env('API_ENDPOINT');

        $response = $client->get($baseUrl.'databyairporthp?dat='.base64_encode(json_encode($request->dat)).'&j='.base64_encode(json_encode($array)));
        $d=json_decode($response->getBody(),true);
        $oficina=[];
        $si=[];
        $no=[];

        for ($i=0; $i < count($d); $i++) {
            array_push($oficina,$d[$i]['descripcion']);
            array_push($si,$d[$i]['si']);
            array_push($no,$d[$i]['no']);
        }

        return array(0=>$oficina,1=>$si,2=>$no);
    }

}
