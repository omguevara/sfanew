<?php

namespace Modules\Votos\app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;



class VotosRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return match($this->method()){
            'POST'=>["tipo_documento"=>"bail|alpha",
            "documento" => "bail|required|numeric|min:6|unique:\Modules\Votos\app\Models\Votos,documento"
            ]
        };
    }

    public function messages()
    {
        return [
            'tipo_documento.alpha'=>'TIPO DE DOCUMENTO. Solo permite letras.',
            'documento.required' => 'DOCUMENTO. Debe introducir el numero de documento.',
            'documento.numeric' => 'DOCUMENTO. Solo permite numeros.',
            'documento.unique'=> 'DOCUMENTO. Ya se realizo un registro con este numero de documento.',
            'documento.min'=> 'DOCUMENTO. Debe tener un minimo de 6 caracteres'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->wantsJson()) {
            $response = [
                'success' => false,
                'message' => '¡Ops! Se produjeron algunos errores',
                'errors' => $validator->errors(),
                'type'=>422
            ];
        } else {
            $response = redirect()->back()->withInput()->withErrors($validator);
        }

        throw new HttpResponseException($response);
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
