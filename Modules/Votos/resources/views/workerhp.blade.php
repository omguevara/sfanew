@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content\_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-12">
            <h2 class="text-center mt-2">Listado de Trabajadores</h2>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <button class="form-control btn btn-success" onclick="VotoE()">Votos Ejercidos</button>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <button class="form-control btn btn-danger" onclick="VotoNE()">Votos No Ejercidos</button>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3">
            <table class="cell-border compact hover order-column" id="ListVotantes">
                <thead>
                    <tr>
                        <th></th>
                        <th>DOCUMENTO</th>
                        <th>NOMBRES</th>
                        <th>UBICACIÓN</th>
                        <th>OFICINA</th>
                        <th>DEPARTAMENTO</th>
                        <th>ESTATUS</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

<script type="text/javascript">
    VotoE();

    function VotoE(){
            $.get({
                url: "{{route('listworkerhpvote')}}",
                data: {_token: token,},
                success: function(resp) {
                    Datatables(resp);
                }
            });
    }

    function VotoNE(){
        $.post({
            url: "{{route('listworkerhpnvote')}}",
            data: {_token: token,},
            success: function(resp) {
                Datatables(resp);
            }
        });
    }

    function Datatables(data){
        $('#ListVotantes').DataTable().destroy();
        var table = $('#ListVotantes').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data,
                language: {
                    url: "{{url('js/dataTable/es.json')}}"
                },
                columns: [
                    {data: 'foto'},
                    {data: function(row){return row.tipo_documento+'-'+row.documento}},
                    {data: function(row){return row.primer_nombre+'-'+row.primer_apellido}},
                    {data: function(row){return row.get_ubicacion.oaci+'-'+row.get_ubicacion.descripcion}},
                    {data: function(row){return row.get_oficina.descripcion}},
                    {data: function(row){return row.get_departamento.descripcion}},
                    {data: 'voto'},
                    {data: 'botones'},
                ]
            });
    }

    function Cne(tip_documento,documento){
        var url=`http://www.cne.gob.ve/web/registro_electoral/ce.php?nacionalidad=${tip_documento}&cedula=${documento}`;
        $('#ModalCne').modal('show');
        $('#cneform').attr('src',url);
    }
</script>

<div class="modal fade" id="ModalCne" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content"style="height:45em;width:35em;">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalCneLabel">Datos del Cne</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <iframe id="cneform" loading="lazy" style="width:100%;height:100%;"></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
@stop

