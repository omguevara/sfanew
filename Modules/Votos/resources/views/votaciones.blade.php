@extends('login::layouts.master')

@section('content')
    <div class="row" id="body">

        <div class="d-flex justify-content-center">
            {{ Form::open(['route'=>'postvotos', 'id' => 'postvotos', 'autocomplete' => 'Off']) }}
            @method('POST')
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                <center><img src="https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png"></center>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('errors'))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ session('errors') }}</li>
                        </ul>
                    </div>
                @endif
            </div>


            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-white">Cédula</div>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 input-group">
                    <div class="input-group-prepend">
                        {{
                            Form::select('tipo_documento', ['V' => 'V', 'E' => 'E','P'=>'P'],'',['class'=>'required form-control','style'=>'width:4em;']);
                        }}
                      </div>
                    {{ Form::text('documento',null, ['data-msg-required'=>'Campo Requerido','class' => 'form-control required', 'id' => 'documento', 'autocomplete'=>'off' ,'maxlength' => "8","onkeypress"=>"return AlphaNumeric(event);"]) }}
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    {{ Form::submit('VOTO EJERCIDO', ['id' => 'IngresoBut', 'class' => 'mt-2 form-control btn btn-info']) }}
                </div>
            </div>
        {{ Form::close() }}
        </div>
    </div>
@endsection
