@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content\_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="row">

        <style>
            @media only screen and (max-width: 767px) {
                /* Tus estilos para teléfonos van aquí */
                #cne{
                    width: 100%;
                    height: 80vh;
                    justify-content: center;
                    align-content: center;
                    background-size:     contain;
                    background-repeat:   no-repeat;
                    background-position: center;
                    background-position: top;
                    display:block;
                    margin: auto;
                    background-image: url({{asset('https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/CNE_logo.svg/2491px-CNE_logo.svg.png')}});*/
                }

                #postvotos{
                    margin-top:24%;
                    border-radius: 31px;
                    margin-left: 2%;
                    background-color: rgba(50, 58, 136, 0.50);
                    height: 47%;
                    width:100%;
                    padding:inherit;
                    box-shadow: #2b2b5e 4px 4px 4px;
                }
            }

            @media only screen and (min-width: 768px) {
                /* Tus estilos para computadoras van aquí */
                #cne{
                    width: 100%;
                    height: 80vh;
                    justify-content: center;
                    align-content: center;
                    background-size:     contain;
                    background-repeat:   no-repeat;
                    background-position: center;
                    background-position: top;
                    display:block;
                    margin: auto;
                    background-image: url({{asset('https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/CNE_logo.svg/2491px-CNE_logo.svg.png')}});*/
                }

                #postvotos{
                    margin-top:4%;
                    border-radius: 31px;
                    margin-left: 20%;
                    background-color: rgba(50, 58, 136, 0.50);
                    height: 47%;
                    width:45%;
                    padding:inherit;
                    box-shadow: #2b2b5e 4px 4px 4px;
                }
            }
        </style>
        <div id="cne">
            {{ Form::open(['route'=>'postvotos', 'id' => 'postvotos', 'autocomplete' => 'Off']) }}
                @method('POST')
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                    <center><img src="https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png"></center>
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('errors'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{ session('errors') }}</li>
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-white">Cédula</div>
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 input-group">
                        <div class="input-group-prepend">
                            {{
                                Form::select('tipo_documento', ['V' => 'V', 'E' => 'E','P'=>'P'],'',['class'=>'required form-control','style'=>'width:4em;']);
                            }}
                        </div>
                        {{ Form::text('documento',null, ['data-msg-required'=>'Campo Requerido','class' => 'form-control required', 'id' => 'documento', 'autocomplete'=>'off' ,'maxlength' => "8","onkeypress"=>"return AlphaNumeric(event);"]) }}
                    </div>

                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        {{ Form::submit('VOTO EJERCIDO', ['id' => 'IngresoBut', 'class' => 'mt-2 form-control btn btn-info']) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
