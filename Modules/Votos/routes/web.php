<?php

use Illuminate\Support\Facades\Route;
use Modules\Votos\app\Http\Controllers\VotosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::match(['get'], 'votaciones', [VotosController::class, 'FormVotaciones'])->name('votaciones');
Route::match(['post'], 'postvotos', [VotosController::class, 'RegistroVoto'])->name('postvotos');

Route::middleware(['auth','MenuProfile'])->group(function(){
    Route::prefix('Votos')->group(function() {
        Route::match(['get'], 'votos', [VotosController::class, 'ListUser'])->name('index');//listado de usuarios que ejercieron el voto

        Route::match(['post'], 'VotantesList', [VotosController::class, 'ListVotantes'])->name('VotantesList');//listado de votantes
        Route::match(['post'], 'listnvotantes', [VotosController::class, 'VotosNoEjercidos'])->name('listnvotantes');//listado de votantes

        Route::match(['post'], 'votoscount', [VotosController::class, 'CountVotos'])->name('votoscount');//listado de votantes
        Route::match(['get'], 'airport', [VotosController::class, 'CountVotesByAirport'])->name('airport');//Listado de aeropuertos}
        Route::match(['post'], 'SearchDataAirport', [VotosController::class, 'BuscarDatosAeropuerto'])->name('SearchDataAirport');//listado de votantes
        Route::match(['get'], 'votaciones', [VotosController::class, 'FormVotaciones'])->name('votaciones');

        Route::match(['get'], 'indexworker', [VotosController::class, 'IndexWorkerHp'])->name('indexworker');//view
        Route::match(['get'], 'listworkerhpvote', [VotosController::class, 'ListWorkerHpVote'])->name('listworkerhpvote');//hp que ejercieron voto
        Route::match(['post'], 'listworkerhpnvote', [VotosController::class, 'ListWorkerHpNoVote'])->name('listworkerhpnvote');//ListWorkerHpNoVote

        Route::match(['post'], 'dataairportbyhp', [VotosController::class, 'GetWorkerHpByAirport'])->name('dataairportbyhp');//ListWorkerHpNoVote

    });
});
