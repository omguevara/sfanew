<?php

namespace Modules\DashBoard\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Votos\app\Http\Controllers\VotosController;

class DashBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $objeto = new VotosController();
        $ubicacion=$objeto->CountVotesByAirport();
        return view('dashboard::index')->with('airport',$ubicacion);
    }
}
