@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content\_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
            <h3>Votos Ejercidos</h3>
            <span id="votoe" style="font-size:80px;"></span>
        </div>

        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
            <h3>Votos No Ejercidos</h3>
            <span id="votone" style="font-size:80px;"></span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
            <h3>Votos Hp Ejercidos</h3>
            <span id="votohpe" style="font-size:80px;"></span>
        </div>

        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
            <h3>Votos Hp No Ejercidos</h3>
            <span id="votohone" style="font-size:80px;"></span>
        </div>
    </div>
    <!--
    <div class="row mt-5">
      <center><h4>TRABAJADORES QUE NO REALIZARON SU VOTO</h4></center>
      <canvas id="myChart"></canvas>
    </div>

    <div class="row mt-5">
      <center><h4>TRABAJADORES QUE REALIZARON SU VOTO</h4></center>
      <canvas id="myChart2"></canvas>
    </div>-->

    <div class="chart-container" id="CountByAirportAll">
      <canvas id="bar-chartcanvas"></canvas>
    </div>

    <div class="row mt-5">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Consulta por aeropuerto</h1>
        </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        {{
          Form::select('airport', $airport,'',['class'=>'required form-control select_chosen','id'=>'airport','placeholder' => 'Seleccione...','onchange'=>'GetAirport($(this).val())']);
        }}
      </div>

      <div class="chart-container" id="DatByAirportChart">
        <canvas id="ChartDataByAirport"></canvas>
      </div>
    </div>

    <div class="row mt-5">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Consultar Hp</h1>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          {{
            Form::select('airporthp', $airport,'',['class'=>'required form-control select_chosen','id'=>'airporthp','placeholder'=>'Seleccione...','onchange'=>'GetDataByAirportHp($(this).val())']);
          }}
        </div>

        <div class="chart-container" id="DivChartAirportHp">
          <canvas id="ChartByAirportHp"></canvas>
        </div>
      </div>

<script type="text/javascript">
    function realizarSolicitud() {
        $.post({
            url: "{{route('votoscount')}}",
            data: {_token: token,},
            success: function(resp) {
            var p = resp.cantforairport;
            GraphyVotos(resp.chartvotone);
            $('#votoe').text(resp.votoe);
            $('#votone').text(resp.votosne);
            $('#votohpe').text(resp.votoshpe);
            $('#votohone').text(resp.votoshpne);
            }
        });
    }

    // Llamar a la función por primera vez
    realizarSolicitud();

    // Llamar a la función cada 10 segundos
    setInterval(realizarSolicitud, 10000);

    /*
        function GraphyAll(dat){//usuarios no han votado
        const ctx = document.getElementById('myChart');
        new Chart(ctx, {
            type: 'bar',
            data: {
            labels: dat[0],
            datasets: [{
                label: '# Cant. Votos',
                data: dat[1],
                borderWidth: 1
            }]
            },
            options: {
            scales: {
                y: {
                beginAtZero: true
                }
            }
            }
        });
        }

        function GraphyJobVotaron(dat){
        const ctx = document.getElementById('myChart2');
        new Chart(ctx, {
            type: 'bar',
            data: {
            labels: dat[0],
            datasets: [{
                label: '# Cant. Votos',
                data: dat[1],
                borderWidth: 1
            }]
            },
            options: {
            scales: {
                y: {
                beginAtZero: true
                }
            }
            }
        });
        }
    */

    function GraphyVotos(dat){
        $('#bar-chartcanvas').remove();
        $('#CountByAirportAll').html('<canvas id="bar-chartcanvas"></canvas>');
        //options
        var options = {
            responsive: true,
            title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
            },
            legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }
            },
            scales: {
            yAxes: [{
                ticks: {
                min: 0
                }
            }]
            }
        };
        //get the bar chart canvas

        var ctx = $("#bar-chartcanvas");

        //bar chart data
        var data = {
            labels: dat.oaci,
            datasets: [
            {
                label: "Voto Ejercido",
                data: dat.votose,
                backgroundColor: [
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)"
                ],
                borderColor: [
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)"
                ],
                borderWidth: 1
            },
            {
                label: "Voto no Ejercido",
                data: dat.votosne,
                backgroundColor: [
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)"
                ],
                borderColor: [
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)"
                ],
                borderWidth: 1
            }
            ]
        };

        //options
        var options = {
            responsive: true,
            title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
            },
            legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#111",
                fontSize: 16
            }
            },
            scales: {
            yAxes: [{
                ticks: {
                min: 0
                }
            }]
            }
        };

        //create Chart class object
        var chart = new Chart(ctx, {
            type: "bar",
            data: data,
            options: options
        });
    }

    function GetAirport(dat){
        $('#ChartDataByAirport').remove();
        $('#DatByAirportChart').html('<canvas id="ChartDataByAirport"></canvas>');
        $.post({
            url: "{{route('SearchDataAirport')}}",
            data: {_token: token,dat:dat},
            success: function(resp) {
                ShowDataJobByAirport(resp);
            }
        });
    }

    function ShowDataJobByAirport(dat){
        //options
        var options = {
            responsive: true,
            title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
            },
            legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }
            },
            scales: {
            yAxes: [{
                ticks: {
                min: 0
                }
            }]
            }
        };
        //get the bar chart canvas
        var ctx = $("#ChartDataByAirport");

        //bar chart data
        var data = {
            labels: dat[0],
            datasets: [
            {
                label: "Voto Ejercido",
                data: dat[1],
                backgroundColor: [
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)"
                ],
                borderColor: [
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)"
                ],
                borderWidth: 1
            },
            {
                label: "Voto no Ejercido",
                data: dat[2],
                backgroundColor: [
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)"
                ],
                borderColor: [
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)"
                ],
                borderWidth: 1
            }
            ]
        };

        //options
        var options = {
            responsive: true,
            title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
            },
            legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#111",
                fontSize: 16
            }
            },
            scales: {
            yAxes: [{
                ticks: {
                min: 0
                }
            }]
            }
        };

        //create Chart class object
        var chart = new Chart(ctx, {
            type: "bar",
            data: data,
            options: options
        });
    }

    function GetDataByAirportHp(dat){
        $('#ChartByAirportHp').remove();
        $('#DivChartAirportHp').html('<canvas id="ChartByAirportHp"></canvas>');
        $.post({
            url: "{{route('dataairportbyhp')}}",
            data: {_token: token,dat:dat},
            success: function(resp) {
            ShowChartDataHp(resp);
            }
        });
    }

    function ShowChartDataHp(dat){

        //options
        var options = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
            fontColor: "#333",
            fontSize: 16
            }
        },
        scales: {
            yAxes: [{
            ticks: {
                min: 0
            }
            }]
        }
        };
        //get the bar chart canvas
        var ctx = $("#ChartByAirportHp");

        //bar chart data
        var data = {
        labels: dat[0],
        datasets: [
            {
            label: "Voto Ejercido",
            data: dat[1],
            backgroundColor: [
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)",
                "rgba(50,150,200,0.3)"
            ],
            borderColor: [
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)",
                "rgba(10,20,30,1)"
            ],
            borderWidth: 1
            },
            {
            label: "Voto no Ejercido",
            data: dat[2],
            backgroundColor: [
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)",
                "rgba(242, 45, 39, 0.8)"
            ],
            borderColor: [
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)",
                "rgba(50,150,200,1)"
            ],
            borderWidth: 1
            }
        ]
        };

        //options
        var options = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            text: "Bar Graph",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
            fontColor: "#111",
            fontSize: 16
            }
        },
        scales: {
            yAxes: [{
            ticks: {
                min: 0
            }
            }]
        }
        };

        //create Chart class object
        var chart = new Chart(ctx, {
        type: "bar",
        data: data,
        options: options
        });
    }
</script>
@stop
