@extends('login::layouts.master')

@section('content')
    <div class="row" id="body">
        <div class="d-flex justify-content-center">
            {{ Form::open(['route'=>'login','id' => 'IngresoForm', 'autocomplete' => 'Off']) }}
            @method('POST')
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                <center><img src="https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png"></center>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                {{-- @if(session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors') }}
                    </div>
                @endif --}}
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        <li>{{ $errors->first() }}</li>
                    </ul>
                </div>
                @endif

            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-2">
                <div class="row form-inline mt-5">
                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                        <h5 class="">USUARIO</h5>
                    </div>

                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::text('user',null, ['data-msg-required'=>'Campo Requerido','class' => 'form-control required', 'id' => 'user', 'autocomplete'=>'off' ,'maxlength' => "30","onkeypress"=>"return AlphaNumeric(event);"]) }}
                            <a class="btn btn-secondary" style="margin-left: -0px;cursor: default;border-top-right-radius: 50px;">
                                <i class="fa fa-user fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 form-inline">
                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                        <h5 class="">CONTRASEÑA</h5>
                    </div>

                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::password('passwd', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'passwd', 'maxlength' => "30"]) }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#passwd').attr('id'))" style="margin-left: -0px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    {{ Form::submit('INGRESAR', ['id' => 'IngresoBut', 'class' => 'mt-2 form-control btn btn-info']) }}
                </div>
            </div>
        {{ Form::close() }}
        </div>
    </div>



  <!-- Modal -->
  <div class="modal fade" id="ModalUsuariosCreate" tabindex="-1" role="dialog" aria-labelledby="ModalUsuariosCreateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalUsuariosCreateLabel">Registro de usuarios</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseModal('ModalUsuariosCreate',event)">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            {{ Form::open(array('id'=>'FormResetPasswd','autocomplete'=>'Off','route'=>'FormPassChange')) }}

                <div class="row mt-2">
                  <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">USUARIO</div>
                  <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                    <div class="">
                      <!--<div class="input-group-prepend">
                        <i class="fa fa-user fa-1x"></i>
                      </div>-->
                        {{
                            Form::text('user','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'user','maxlength'=>20]);
                        }}
                    </div>
                  </div>
                </div>

                <div class="row mt-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button id="ButtFormResetPasswd" name="ButtFormResetPasswd" onclick="return SendResetPasswd(event)" class="form-control btn btn-success">
                        Solicitar Cambio de Contraseña
                    </button>
                  </div>
                </div>

              {{ Form::close() }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="return CloseModal('ModalUsuariosCreate',event)">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection
