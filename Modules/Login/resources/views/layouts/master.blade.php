<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Login Module - {{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="{{ $description ?? '' }}">
    <meta name="keywords" content="{{ $keywords ?? '' }}">
    <meta name="author" content="{{ $author ?? '' }}">
    <!-- Fonts -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/js/all.min.js" integrity="sha512-GWzVrcGlo0TxTRvz9ttioyYJ+Wwk9Ck0G81D+eO63BaqHaJ3YZX9wuqjwgfcV/MrB2PhaVX9DkYVhbFpStnqpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('js/formValidate/jquery.validate.min.js') }}"></script>

    <script src="{{asset('js/login.js')}}"></script>
    <script src="{{asset('js/botones.js')}}"></script>
    @vite(['resources/js/app.js', 'resources/css/app.scss'])
    {{-- Vite CSS --}}
    {{-- {{ module_vite('build-login', 'resources/assets/sass/app.scss') }} --}}
    <style>

        @media only screen and (max-width: 767px) {
            /* Tus estilos para teléfonos van aquí */
            #body{
                background-image: url({{asset('img/1459.jpg')}});
                background-repeat:   no-repeat;
                background-position: center;
                height:100vh;
            }
            form{
                margin-top:40%;
                border-radius: 31px;
                background-color:  rgba(50, 58, 136, 0.50);
                height: 48vh;
                padding:inherit;
                position: absolute;
                box-shadow: #2b2b5e 4px 4px 4px;
            }
            #banner{
                background-image: url({{asset('https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png')}});
                background-repeat:   no-repeat;
                background-position: center;
                height: 12vh;
            }

        }

        @media only screen and (min-width: 768px) {
            /* Tus estilos para computadoras van aquí */
            #body{
                background-image: url({{asset('img/airport-terminal1.jpg')}});
                background-size:     cover;
                background-repeat:   no-repeat;
                background-position: center center;
                background-position: top;
                height:100vh;
            }
            form{
                margin-top:15%;
                border-radius: 31px;
                background-color: rgba(50, 58, 136, 0.50);
                height: 47%;
                width:45%;
                padding:inherit;
                box-shadow: #2b2b5e 4px 4px 4px;
            }

            #banner{
                /* background-image: url({{asset('https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png')}}); */
                background-repeat:   no-repeat;
                background-position: center;
                height: 50%;
                position: absolute;
                min-width: 100vh;
            }

            /* #IngresoForm{
                background-color: rgba(50, 58, 136, 0.50);
                margin-top: 12%;
                height: 33%;
                width:39%;
                border-radius:8%;
                padding: inherit;
            } */
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        @yield('content')
    </div>
    {{-- Vite JS --}}
    {{-- {{ module_vite('build-login', 'resources/assets/js/app.js') }} --}}
</body>
