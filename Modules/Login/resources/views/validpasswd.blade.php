@extends('login::layouts.master')

@section('content')

<div class="row" id="body">
    <div class="d-flex justify-content-center">
        {{ Form::open(['route'=>'ValidPass','id' => 'ValidPass', 'autocomplete' => 'Off']) }}
            @method('PUT')
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

                <div class="row mt-2 form-inline">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li><b>{{ $error }}</b></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row mt-2 form-inline">
                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                        <h5 class="">CONTRASEÑA ENVIADA AL CORREO</h5>
                    </div>

                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::password('PasswdAct', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'PasswdAct', 'maxlength' => "30"]) }}
                            {{ Form::text('i',$id, ['data-msg-required'=>'Campo Requerido','class' => 'form-control required', 'id' => 'i','readonly','hidden']) }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#PasswdAct').attr('id'))" style="margin-left: -0px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 form-inline">
                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                        <h5 class="">NUEVA CONTRASEÑA</h5>
                    </div>

                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::password('NewPass', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'NewPass', 'maxlength' => "30"]) }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#NewPass').attr('id'))" style="margin-left: -10px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 form-inline">
                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                        <h5 class="">REPETIR NUEVA CONTRASEÑA</h5>
                    </div>

                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::password('RepPasswd', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'RepPasswd', 'maxlength' => "30"]) }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#RepPasswd').attr('id'))" style="margin-left: -0px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="row mt-3">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        {{ Form::submit('CAMBIAR CONTRASEÑA', ['id' => 'ButtChangePass', 'class' => 'form-control btn btn-info mb-2']) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
<script src="{{asset('js/login/ChangePass.js')}}"></script>
@endsection
