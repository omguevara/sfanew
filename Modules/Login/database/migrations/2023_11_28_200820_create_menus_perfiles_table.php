<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menus_perfiles', function (Blueprint $table) {
            $table->id();
            $table->integer('perfil_id');
            $table->integer('menu_id');
            $table->boolean('status')->default(true);
            $table->string('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['perfil_id','menu_id']);
            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->foreign('menu_id')->references('id')->on('menus');
        });

        DB::table('menus_perfiles')->insert([
            [
                'menu_id'=>2,
                'perfil_id'=>'1'
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menus_perfiles');
    }
};
