<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clasificaciones', function (Blueprint $table) {
            $table->id();
            $table->string('name_clasificacion')->unique();
            $table->decimal('porcentaje');
            $table->boolean('status')->default(true);
            $table->integer('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['name_clasificacion']);
        });

        DB::table('clasificaciones')->insert([
            [
                'name_clasificacion'=>'CATEGORÍA A',
                'porcentaje'=>1,
            ],
            [
                'name_clasificacion'=>'CATEGORÍA B',
                'porcentaje'=>0.85,
            ],
            [
                'name_clasificacion'=>'CATEGORÍA C',
                'porcentaje'=>0.6,
            ],
            [
                'name_clasificacion'=>'CATEGORÍA D',
                'porcentaje'=>0.3,
            ],
            [
                'name_clasificacion'=>'CAREGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO A',
                'porcentaje'=>0.9,
            ],
            [
                'name_clasificacion'=>'CAREGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO B',
                'porcentaje'=>0.8,
            ],
            [
                'name_clasificacion'=>'CAREGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO C',
                'porcentaje'=>0.6,
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clasificaciones');
    }
};
