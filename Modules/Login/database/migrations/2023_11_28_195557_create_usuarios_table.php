<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->string('tipo_documento');
            $table->string('documento')->unique();
            $table->string('p_nombre');
            $table->string('s_nombre');
            $table->string('p_apellido');
            $table->string('s_apellido')->nullable();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('tlf');
            $table->integer('perfil_id');
            $table->boolean('verificado')->default(false);
            $table->boolean('activo')->default(true);
            $table->boolean('change_password')->default(false);
            $table->string('ip');
            $table->integer('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->index(['documento','email','username','tlf']);
        });

        DB::table('usuarios')->insert([
            [
                'tipo_documento'=>'V',
                'documento'=>'12345678',
                'p_nombre'=>'ADMIN',
                's_nombre'=>'BAER',
                'p_apellido'=>'OTIC',
                's_apellido'=>'BAER',
                'email'=>'baer@baer.gob.ve',
                'username'=>'admintic',
                'password'=>Hash::make('123456789'),
                'tlf'=>'012345678912',
                'perfil_id'=>1,
                'ip'=>'127.0.0.1',
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuarios');
    }
};
