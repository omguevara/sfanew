<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('slot', function (Blueprint $table) {
            $table->id();
            $table->integer('aeropuertos_id');
            $table->integer('num_slot');
            $table->boolean('status')->default(true);
            $table->string('ip');
            $table->integer('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('aeropuertos_id')->references('id')->on('aeropuertos');
            $table->index(['num_slot','aeropuertos_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('slot');
    }
};
