<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuarios_aeropuertos', function (Blueprint $table) {
            $table->id();
            $table->integer('usuario_id');
            $table->integer('aeropuerto_id');
            $table->boolean('activo')->default(true);
            $table->string('ip');
            $table->integer('usuario_add_id')->nullable();;
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            //$table->foreign('aeropuerto_id')->references('id')->on('aeropuertos');
        });

        DB::table('usuarios_aeropuertos')->insert([
            [
                'usuario_id'=>'1',
                'aeropuerto_id'=>'65',
                'ip'=>'127.0.0.1'
            ]
        ]);


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuarios_aeropuertos');
    }
};
