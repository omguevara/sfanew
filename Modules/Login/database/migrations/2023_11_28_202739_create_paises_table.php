<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->id();
            $table->string('ruta_img');
            $table->string('pais')->unique();
            $table->string('abreviatura')->unique();
            $table->string('cod_tlf')->unique();
            $table->string('ip');
            $table->boolean('activo')->default(true);
            $table->string('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['pais','abreviatura']);
        });

        DB::table('paises')->insert([
            [
                'ruta_img'=>'VE.jpg',
                'pais'=>'REPÚBLICA BOLIVARIANA DE VENEZUELA',
                'abreviatura'=>'VE',
                'cod_tlf'=>'+58',
                'ip'=>'127.0.0.1',
                'activo'=>true
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paises');
    }
};
