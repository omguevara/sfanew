<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('aeropuertos', function (Blueprint $table) {
            $table->id();
            $table->string('cod_oaci',4)->unique();
            $table->string('cod_iata',3)->unique();
            $table->string('name_aeropuerto')->unique();
            $table->integer('clasificacion_id');
            $table->boolean('administrado')->default(false);
            $table->decimal('iva');
            $table->time('ocaso');
            $table->time('alba');
            $table->integer('pais_id');
            $table->boolean('activo')->default(true);
            $table->string('ip');
            $table->string('usuario_id')->nullable();
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('clasificacion_id')->references('id')->on('clasificaciones');
            $table->foreign('pais_id')->references('id')->on('paises');
            $table->index(['cod_oaci','cod_iata','name_aeropuerto']);
        });

        DB::table('aeropuertos')->insert([
            [
                'cod_oaci'=>'SVMG',
                'cod_iata'=>'PMV',
                'name_aeropuerto'=>'G/J SANTIAGO MARIÑO',
                'clasificacion_id'=>5,
                'administrado'=>TRUE,
                'pais_id'=>1,
                'ip'=>'127.0.0.1',
                'ocaso'=>'18:00',
                'alba'=>'06:00',
                'iva'=>'0.16'
            ],
            [
                'cod_oaci'=>'SVMC',
                'cod_iata'=>'MAR',
                'name_aeropuerto'=>'AEROPUERTO INTERNACIONAL LA CHINITA',
                'clasificacion_id'=>2,
                'administrado'=>TRUE,
                'pais_id'=>1,
                'ip'=>'127.0.0.1',
                'ocaso'=>'18:30',
                'alba'=>'05:30',
                'iva'=>'1'
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('aeropuertos');
    }
};
