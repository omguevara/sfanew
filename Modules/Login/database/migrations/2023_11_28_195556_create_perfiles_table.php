<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('perfiles', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->unique();
            $table->string('ip');
            $table->boolean('create');
            $table->boolean('update');
            $table->boolean('delete');
            $table->boolean('status')->default(true);
            $table->string('usuario_id');
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['descripcion']);
        });
        DB::table('perfiles')->insert([
            [
                'descripcion'=>'ADMINISTRADOR',
                'ip'=>'127.0.0.1',
                'usuario_id'=>1,
                'create'=>true,
                'update'=>true,
                'delete'=>true,
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('perfiles');
    }
};
