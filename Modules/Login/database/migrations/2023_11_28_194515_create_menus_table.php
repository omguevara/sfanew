<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->text('icono');
            $table->text('ruta');
            $table->text('name_menu')->unique();
            $table->boolean('status')->default(true);
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->index(['name_menu']);
        });

        DB::table('menus')->insert([
            [
                'icono'=>'fas fa-tachometer-alt',
                'ruta' => 'dashboard/index',
                'name_menu'=>'DASHBOARD',
            ],[
                'icono'=>'fas fa-vote-yea',
                'ruta' => 'Votos/votaciones',
                'name_menu'=>'ANUNCIAR VOTACIONES',
            ],[
                'icono'=>'fas fa-users',
                'ruta' => 'Votos/votos',
                'name_menu'=>'VOTANTES',
            ],[
                'icono'=>'fas fa-vote-yea',
                'ruta' => 'Votos/indexworker',
                'name_menu'=>'VOTANTES HP',
            ],[
                'icono'=>'fas fa-user',
                'ruta' => 'Usuarios/index',
                'name_menu'=>'USUARIOS',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};
