<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('log_login', function (Blueprint $table) {
            $table->id();
            $table->integer('usuario_id');
            $table->string('token');
            $table->string('browsern');//explorador
            $table->string('browserf');//kernel
            $table->string('platform');//plataforma
            $table->string('ip');
            $table->boolean('status')->default(true);
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->index(['usuario_id','ip']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('log_login');
    }
};
