<?php

namespace Modules\Login\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Login\Database\factories\UsuariosFactory;
use \App\Traits\EncryptationId;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;






class Usuarios extends Authenticatable
{
    use HasFactory,EncryptationId;

    /**
     * The attributes that are mass assignable.
     */
    protected $table ='usuarios';
    public $timestamps = false;
    protected $appends = ['crypt_id'];//,'ubicacion','oficinas'];
    protected $fillable = ['p_nombre'];//'sede_id','oficina_id','user_id'];
    protected $hidden = ['id','password',''];

    protected static function newFactory(): UsuariosFactory
    {
        //return UsuariosFactory::new();
    }
}
