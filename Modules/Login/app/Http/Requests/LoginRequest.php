<?php

namespace Modules\Login\app\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rules\Password;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array

    {
        return match($this->method()){
            'PUT'=>[
                "PasswdAct" => "bail|required",
                "NewPass"=>["required","max:15",
                    Password::min(8)->letters()->numbers()->symbols()->mixedCase()
                ],
                "RepPasswd"=>"bail|string|required|same:NewPass",
            ]
        };
    }

    public function messages()
    {
        return [
            'PasswdAct.required' => 'CONTRASEÑA ENVIADA AL CORREO. Debe introducir la contraseña enviada a su correo.',

            'NewPass.required' => 'NUEVA CONTRASEÑA. Debe introducir SU NUEVA CONTRASEÑA.',
            'NewPass.min' => 'NUEVA CONTRASEÑA. Debe introducir un minimo de 8 caracteres.',
            'NewPass.max' => 'NUEVA CONTRASEÑA. Debe introducir un maximo de 15 caracteres.',

            'password.min' => 'NUEVA CONTRASEÑA. Debe contener solo carateres permitidos.',
            'password.letters' => 'NUEVA CONTRASEÑA. Debe contener al menos una letra.',
            'password.numbers' => 'NUEVA CONTRASEÑA. Debe contener al menos un número.',
            'password.symbols' => 'NUEVA CONTRASEÑA. Debe contener al menos un símbolo.',
            'password.mixed' => 'NUEVA CONTRASEÑA. Debe contener al menos una letra mayúscula y una minúscula.',

            'RepPasswd.required' => 'REPETIR NUEVA CONTRASEÑA. Debe introducir SU NUEVA CONTRASEÑA.',
            'RepPasswd.same' => 'REPETIR NUEVA CONTRASEÑA. Las contraseña no son iguales.',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        if ($this->wantsJson()) {
            $response = [
                'success' => false,
                'message' => '¡Ops! Se produjeron algunos errores',
                'errors' => $validator->errors(),
                'type'=>422
            ];
        } else {
            $response = redirect()->back()->withInput()->withErrors($validator);
        }

        throw new HttpResponseException($response);
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
