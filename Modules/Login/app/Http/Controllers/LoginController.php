<?php

namespace Modules\Login\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Login\app\Models\Usuarios;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Browser;
use Modules\Login\app\Http\Requests\LoginRequest;
use Illuminate\Validation\Rules\Password;
use \Modules\Aeropuertos\app\Models\AeropuertosUsuarios;

class LoginController extends Controller
{
    protected $user;
    public function __construct() {
        $this->user = new Usuarios();
    }

    /**
     * Display a listing of the resource.
     */

    public function index(){
        if(Auth::check()==true){
            return redirect('dashboard/index');
        }
        return view('login::index');
    }


    public function Login(Request $request){
        $request->validate(
            [
                'user' => 'bail|required|alpha_num|min:5',
                'passwd' => 'bail|required|min:8',
                //'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
            ],
            [
                'user.required' => 'USUARIO O CONTRASEÑA INCORRECTO',
                'user.alpha_num' => 'EL NOMBRE DE USUARIO NO PUEDE CONTENER CARACTERES ESPECIALES',
                'user.min' => 'USUARIO O CONTRASEÑA INCORRECTO',
                'passwd.required' => 'CORREO O CONTRASEÑA INCORRECTO',
                'passwd.min' => 'USUARIO O CONTRASEÑA INCORRECTO'
                //'g-recaptcha-response.recaptchav3' => 'CAPTCHA OBLIGATORIO'
            ]
        );
		$login = array('username' => strtolower($request->user),'password' => $request->passwd);

        $usuario=$this->user::where('username',$login['username'])->first();
        if($this->user::where('username',$login['username'])->exists() && Hash::check($login['password'],$usuario->password)){//consulta si el usuario existe
            if($usuario->activo==true && $usuario->verificado == true && $usuario->change_password==true){//valida que el usuario este activo y verificado
                if(Auth::attempt($login)==true){//ingresar al sistema.
                    $aeropuerto=AeropuertosUsuarios::where('usuario_id',$usuario->id)->where('activo',true)->get();
                    $airport=[];
                    for ($i=0; $i < count($aeropuerto); $i++) {
                        array_push($airport,$aeropuerto[$i]['aeropuerto_id']);
                    }
                    $request->session()->put('airport', $airport);
                    return redirect('dashboard/index');//->with($type, $mensaje);
                }
                /*}else{
                    return $this->RedirectIndex('error','Debe revisar la bandeja de entradas y verificar su registro');
                }*/
            }else{
                if($usuario->activo==true && $usuario->verificado == false && $usuario->change_password==false){
                    return redirect(route('ChangePass',\App\Helpers\Encryptor::encrypt($usuario->id)));
                }elseif($usuario->activo==false){
                    return $this->RedirectIndex('error','Error contacte con el administrador del sistema');
                }
            }
        }else{
            return $this->RedirectIndex('error','Usuario o contraseña equivocado.');
        }
    }

    private function RedirectIndex($type,$mensaje){
        return redirect()->route('index')->with($type, $mensaje);
        /*crear registro de log de ingresos por ip*/
    }

    public function Logout(Request $request){
		Auth::logout();
		return redirect('Login/index');
    }
    private function AddLogEntrance(){
        //Registrara los intentos de inicio de session
    }

    // validara que el usuario este activo y que no realizo el cambio de contraseña
    public function ValidAccountUser($id){
        $user=$this->user::find(\App\Helpers\Encryptor::decrypt($id));
        if($user->activo==true && $user->verificado==false){
            return view('login::validpasswd')->with('id',$id);
        }
    }

    public function FirstChangePass(LoginRequest $request){//primer cambio de clave
        $use=$this->user::find(\App\Helpers\Encryptor::decrypt($request->i));
        /*
            verifica si el usuario no ha cambiado clave y la verificacion al cambiar la clave esta en false
        */
        if($use->verificado==false && $use->change_password==false){
            if(Hash::check($request->PasswdAct,$use->password)){
                $this->user::where('id',$use->id)->
                update([
                    "password"=>Hash::make($request->NewPass),
                    "verificado" => true,
                    "change_password" => true
                ]);
                return redirect('Login/index')->with('success','Cambio de clave realizado');
            }else{
                return $this->RedirectIndex('error','Error.');
            }
        }else{
            return $this->RedirectIndex('error','Error.');
        }
    }

    public function ChangePassForm(Request $request){//
        $request->validate(
            [
                'user' => 'bail|required|alpha_num|min:5',
            ],
            [
                'user.required' => 'DEBE INGRESAR EL NOMBRE DE USUARIO',
                'user.alpha_num' => 'EL NOMBRE DE USUARIO NO PUEDE CONTENER CARACTERES ESPECIALES',
                'user.min' => 'DEBE INGRESAR UN MINIMO DE 5 CARACTERES'
            ]
        );
    }
}
