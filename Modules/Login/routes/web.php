<?php

use Illuminate\Support\Facades\Route;
use Modules\Login\app\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::group([], function () {
Route::prefix('Login')->group(function() {
    // Route::resource('', LoginController::class)->names('index');
    Route::match(['post','get'],'index', 'LoginController@index')->name('index');//form enter
    Route::match(['post','get'],'login', 'LoginController@Login')->name('login');//valid form
    Route::match(['post','get'],'logout', 'LoginController@Logout')->name('logout');//exit system

    Route::match(['get'],'ChangePass/{id}', 'LoginController@ValidAccountUser')->name('ChangePass');//form first change pass
    Route::match(['put'],'ValidPass', 'LoginController@FirstChangePass')->name('ValidPass');//valid data for change first pass

    Route::match(['post'],'FormPassChange', 'LoginController@ChangePassForm')->name('FormPassChange');//ChangePassByUsuario
    //Route::match(['put'],'RecovPasswd', 'LoginController@RecoverPassByUser')->name('RecovPasswd');//ChangePassByUsuario
});
