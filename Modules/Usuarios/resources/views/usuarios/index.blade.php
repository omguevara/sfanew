{{-- @extends('dashboard::layouts.master')

@section('content')
    <h1>Hello World</h1>

    <p>Module: {!! config('dashboard.name') !!}</p>
@endsection --}}





@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content\_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')


    <div class="row">
        <div class="col-12">
            <h2 class="text-center mt-2">Listado de usuarios</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button class="btn btn-info form-control" onclick="$('#ModalUsuariosCreate').modal('show')"><i class="fa fa-user-plus mg-3"></i>Registrar Usuarios</button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3">
            <table class="cell-border compact hover order-column" id="UserList">
                <thead>
                    <tr>
                        <th>DOCUMENTO</th>
                        <th>NOMBRES</th>
                        <th>ESTATUS</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        var dat = @json($USUARIOS);
        var table = $('#UserList').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: dat,
            language: {
                url: "{{url('js/dataTable/es.json')}}"
            },
            columns: [
                {data: 'documento'},
                {data: 'nombres'},
                {data: 'status'},
                {data: 'botones'}
            ]
        });
    </script>

    @include('usuarios::usuarios.Register')
    @include('usuarios::usuarios.Update')
    <script src="{{ asset('js/usuarios/UsuariosRegister.js') }}" ></script>
    <script src="{{ asset('js/usuarios/UsuariosUpdate.js') }}" ></script>
    <script src="{{ asset('js/usuarios/UsuariosDelete.js') }}" ></script>
@stop

