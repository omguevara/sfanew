<div class="modal fade ModalUsuariosCreate" id="ModalUsuariosCreate" tabindex="-1" role="dialog" aria-labelledby="ModalUsuariosCreateLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalUsuariosCreateLabel">Registro de usuarios</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseModal('ModalUsuariosCreate',event)">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            {{ Form::open(array('id'=>'FormAddUser','autocomplete'=>'Off','route'=>'PostUser')) }}

                @method('POST')

                <div class="row form-group">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                    <h6><b>DOCUMENTO</b></h6>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 form-inline">
                    {{
                        Form::select('tipo_documento', ['V' => 'V', 'E' => 'E','P'=>'P'],'',['class'=>'form-control form-select required']);
                    }}

                    {{
                        Form::text('documento','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'documento','maxlength'=>50]);
                    }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Primer Nombre</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                            {{
                                Form::text('pnombre','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'pnombre','maxlength'=>50]);
                            }}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Segundo Nombre</div>
                          <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                              {{
                                  Form::text('snombre','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'snombre','maxlength'=>50]);
                              }}
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Primer Apellido</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                            {{
                                Form::text('papellido','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'papellido','maxlength'=>50]);
                            }}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Segundo Apellido</div>
                          <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                              {{
                                  Form::text('sapellido','',['class' => 'form-control','id'=>'sapellido','maxlength'=>50]);
                              }}
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Teléfono</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-phone"></i></span>
                            </div>
                            {{
                                Form::text('telefono','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required telefono','id'=>'telefono','maxlength'=>50]);
                            }}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Usuario</div>
                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-user-tie"></i></span>
                                    </div>
                                    {{
                                        Form::text('usuario','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'usuario','maxlength'=>50]);
                                    }}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Correo</div>
                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend2">@</span>
                            </div>
                            {{
                                Form::text('correo','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'correo','maxlength'=>50]);
                            }}
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Perfil</div>
                    <div class="col-lg-10 col-md-10 col-xs-12 col-sm-12">
                        <div class="input-group">
                            <div class="col-lg-1 col-md-1 col-xs-2 col-sm-2">
                                <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-address-card"></i></span>
                            </div>
                            <div class="col-lg-11 col-md-11 col-xs-10 col-sm-10">
                                {{
                                    Form::select('perfil',$perfiles, null, ['data-msg-required'=>'Campo requerido','id'=>'perfil','class'=>'select_chosen form-control']);
                                }}
                            </div>
                            {{-- <div class="input-group-append">

                            </div> --}}
                        </div>
                  </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Ubicación</div>
                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        {{
                            Form::select('aeropuerto[]',$aeropuerto, null, ['data-msg-required'=>'Campo requerido','id'=>'aeropuerto','class'=>'select_chosen form-control',"multiple"]);
                        }}
                    </div>
                </div>
                <div class="row mt-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button id="RegistrarUsuario" name="RegistrarUsuario" class="form-control btn btn-success">Registrar</button>
                  </div>
                </div>

              {{ Form::close() }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="return CloseModal('ModalUsuariosCreate',event)">Close</button>
        </div>
      </div>
    </div>
  </div>
