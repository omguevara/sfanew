<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <img src="https://www.baer.gob.ve/wp-content/uploads/2022/10/logo-baer.png" class="img-fluid">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <p>
                Estimado/a {{$nombres}}, Nos complace informarle que su cuenta ha sido creada con éxito. Ahora puede acceder
                al sistema y disfrutar de todos nuestros servicios.
                Su Usuario es <b>{{$user}}</b> y su contraseña por defecto es <b>{{$pwd}}</b>.
                Le recomendamos que cambie su contraseña lo antes posible para garantizar la seguridad de su cuenta.
                <br>Ingresará al sistema <a href="{{url(env('APP_URL'))}}" class="btn btn-info">AQUÍ</a><br>
                Si tiene alguna pregunta o necesita ayuda, no dude en ponerse en contacto con nuestro equipo de soporte otic.baer@gmail.com

                Estamos a su disposición para ayudarlo en todo lo que necesite. Gracias por unirse a nuestra comunidad. Atentamente,
                BOLIVARIANA DE AEROPUERTOS.
            </p>
        </div>
    </div>
</body>
</html>
