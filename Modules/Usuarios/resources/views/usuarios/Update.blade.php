<div class="modal fade ModalUsuariosUpdate" id="ModalUsuariosUpdate" tabindex="-1" role="dialog" aria-labelledby="ModalUsuariosUpdateLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalUsuariosUpdateLabel">Actualizar datos del usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseModal('ModalUsuariosUpdate',event)">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>


        <div class="modal-body">
            {{ Form::open(array('id'=>'FormPutUser','autocomplete'=>'Off','route'=>'postusers')) }}
            @method('PUT')
                <div class="row form-group">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                    <h6><b>DOCUMENTO</b></h6>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 form-inline">
                    {{
                        Form::select('tipo_documento', ['V' => 'V', 'E' => 'E'],'',['class'=>'form-control form-select required','id'=>'tipo_documento']);
                    }}

                    {{
                        Form::text('documento','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'documento','maxlength'=>9]);
                    }}
                    {{
                        Form::text('id','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'id','maxlength'=>100,'readonly','hidden']);
                    }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Primer Nombre</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                            {{
                                Form::text('pnombre','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'pnombre','maxlength'=>50]);
                            }}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Segundo Nombre</div>
                          <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                              {{
                                  Form::text('snombre','',['class' => 'form-control','id'=>'snombre','maxlength'=>50]);
                              }}
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Primer Apellido</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                            {{
                                Form::text('papellido','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'papellido','maxlength'=>50]);
                            }}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Segundo Apellido</div>
                          <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                              {{
                                  Form::text('sapellido','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'sapellido','maxlength'=>50]);
                              }}
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Teléfono</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-phone"></i></span>
                            </div>
                            {{
                                Form::text('telefono','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required telefono','id'=>'telefono','maxlength'=>50]);
                            }}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Usuario</div>
                              <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-user-tie"></i></span>
                                  </div>
                                  {{
                                      Form::text('usuario','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'usuario','maxlength'=>50,'readonly','disabled']);
                                  }}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                  <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Perfil</div>
                  <div class="col-lg-10 col-md-10 col-xs-12 col-sm-12">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-address-card"></i></span>
                      </div>
                        {{
                            Form::select('perfil',$perfiles, null, ['data-msg-required'=>'Campo requerido','placeholder' => 'Seleccione...','id'=>'perfil','class'=>'form-control']);
                        }}
                    </div>
                  </div>
                </div>

                <div class="row mt-2">
                  <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Correo</div>
                  <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend2">@</span>
                      </div>
                        {{
                            Form::text('correo','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'correo','maxlength'=>50,'readonly','disabled']);
                        }}
                    </div>
                  </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">Ubicación</div>
                    <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                        {{
                            Form::select('aeropuerto[]',$aeropuerto, null, ['id'=>'aeropuerto','class'=>'select_chosen',"multiple","style"=>"width:100vw;"]);
                        }}
                    </div>
                </div>

                <div class="row mt-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button id="PutUsers" name="PutUsers" class="form-control btn btn-success" onclick="return ButtPutUser($('#FormPutUser').serializeObject(),event)"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                  </div>
                </div>

              {{ Form::close() }}

            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="return CloseModal('ModalUsuariosUpdate',event)">Close</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="ModalPutStatusUsuarios" tabindex="-1" role="dialog" aria-labelledby="ModalPutStatusUsuariosLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalUsuariosUpdateLabel">Actualizar datos del usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseModal('ModalPutStatusUsuarios',event)">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            {{ Form::open(array('id'=>'FormPutUser','autocomplete'=>'Off','route'=>'postusers')) }}

                <div class="row form-group">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                    <h6><b>DOCUMENTO</b></h6>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 form-inline">
                        <label class="font-weight-bold" id="DocumentoDel"></label>
                    {{
                        Form::text('put_id','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'put_id','maxlength'=>100,'readonly']);
                    }}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                    <h6><b>NOMBRES</b></h6>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12 form-inline">
                        <label class="font-weight-bold" id="NombresDel"></label>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Teléfono</div>
                        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-phone"></i></span>
                            </div>
                            <label class="font-weight-bold" id="TelefonoDel"></label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="row">
                              <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">Usuario</div>
                              <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-user-tie"></i></span>
                                  </div>
                                  <label class="font-weight-bold" id="UsersDel"></label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>



                <div class="row mt-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button id="PutUsers" name="PutUsers" class="form-control btn btn-success" onclick="return ButtPutUser($('#FormPutUser').serializeObject(),event)"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                  </div>
                </div>
              {{ Form::close() }}
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="return CloseModal('ModalPutStatusUsuarios',event)">Close</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="ModalChangePassword" tabindex="-1" role="dialog" aria-labelledby="ModalChangePasswordLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-center" id="ModalUsuariosUpdateLabel">Actualizar datos del usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseModal('ModalChangePassword',event)">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            {{ Form::open(array('id'=>'FormPutUser','autocomplete'=>'Off','route'=>'changepass')) }}
            @method('PUT')
                <div class="row mt-2 form-inline">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                        <h5 class="">NUEVA CONTRASEÑA</h5>
                    </div>

                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                        <div class="input-group">

                            {{
                                Form::text('id','',['data-msg-required'=>'Campo requerido','class' => 'form-control  required','id'=>'id','readonly','hidden']);
                            }}
                            {{
                                Form::password('passwd', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'passwd', 'maxlength' => "30"]),
                            }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#passwd').attr('id'))" style="margin-left: -0px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 form-inline">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                        <h5 class="">CONFIRMAR CONTRASEÑA</h5>
                    </div>

                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                        <div class="input-group">
                            {{ Form::password('rpasswd', ['data-msg-required'=>'Campo Requerido','class' => 'form-control required BtnChangeType', 'id' => 'rpasswd', 'maxlength' => "30"]) }}
                            <a class="btn btn-secondary ShowPassIco" onclick="ShowPasswd(event,$('#rpasswd').attr('id'))" style="margin-left: -0px;cursor: pointer;border-top-right-radius: 50px;">
                                <i class="passwdico fa fa-solid fa-eye fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <button class="form-control btn btn-info"><i class="fas fa-unlock-alt px-2"></i>Cambiar Contraseña</button>
                    </div>
                </div>

            {{ Form::close() }}

            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="return CloseModal('ModalChangePassword',event)">Close</button>
        </div>
      </div>
    </div>
</div>
