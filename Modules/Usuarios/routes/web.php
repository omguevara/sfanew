<?php

use Illuminate\Support\Facades\Route;
use Modules\Usuarios\app\Http\Controllers\UsuariosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth','MenuProfile'])->group(function(){
    Route::prefix('Usuarios')->group(function() {
        Route::resource('index', UsuariosController::class)->names('index');
        Route::match(['post'], 'PostUser', [UsuariosController::class, 'CrearUsuarios'])->name('PostUser');
        Route::match(['get'], 'getusers/{id}', [UsuariosController::class, 'ConsultarUsuario'])->name('getusers');
        Route::match(['put'], 'postusers', [UsuariosController::class, 'ActualizarUsuario'])->name('postusers');
        Route::match(['get'], 'putstatususers/{id}', [UsuariosController::class, 'CambiarStatusUsuario'])->name('putstatususers');

        Route::match(['put'], 'changepass', [UsuariosController::class, 'CambiarPasswdAdmin'])->name('changepass');
    });
});
