<?php

namespace Modules\Usuarios\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use \Modules\Usuarios\app\Models\Usuarios;
use \Modules\Aeropuertos\app\Models\Aeropuertos;
use \Modules\Aeropuertos\app\Models\AeropuertosUsuarios;
use \Modules\Usuarios\app\Http\Requests\UsuariosRequest;
use \Modules\Usuarios\app\Http\Requests\ChangePassRequest;
use \Modules\Perfiles\app\Models\Perfiles;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DataTables;
use Browser;
use Mail;
use App\Mail\SendMail;
use GuzzleHttp\Client;

class UsuariosController extends Controller
{
    protected $usuario;
    protected $perfiles;
    protected $date;
    protected $airportuser;
    public function __construct() {
        $this->usuario = new Usuarios();
        $this->perfiles = new Perfiles();
        $this->airportuser= new AeropuertosUsuarios();
        $this->date=Carbon::now();
    }

    /**
     * Display a listing of the resource.
     */
    public function index(){
        /*$subject = "Registro de Piloto";
        $for = "makeisi638@gmail.com";
        $datos = $new->toArray();
        $datos['pwd'] = "123456789";
        Mail::send('email.registro_piloto', $datos, function ($msj) use ($subject, $for) {
            $msj->subject($subject);
            $msj->to($for);
        });*/
        //$aeropuertos=Aeropuertos::get()->pluck('name_aeropuerto','crypt_id');

        $aeropuertos=$this->AirportFicha();

        $usuarios=$this->usuario::join('perfiles','perfiles.id','=','usuarios.perfil_id')
        ->where('usuarios.id','<>',Auth::id())
        ->selectRaw("CONCAT(usuarios.tipo_documento, '-', usuarios.documento) AS documento,usuarios.id,CONCAT(usuarios.p_nombre,' ',usuarios.p_apellido) AS nombres,username,activo,perfiles.descripcion")
        ->get();
        $perfiles=$this->perfiles::where('status',true)->get()->pluck('descripcion', 'crypt_id');

        $data = Datatables::of($usuarios)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    if($row->activo==true){
                        $sms="Activo";
                    }else{
                        $sms="Inactivo";
                    }
                    return $sms;
                })
                ->addColumn('botones', function ($row) {
                    if($row->activo==true){
                        //activo
                        $class="btn btn-outline-success";
                        $title="Inactivar usuario";
                        $btn="fa fa-user-check";
                    }else{
                        //si esta inactivo
                        $class="btn btn-outline-danger";
                        $title="Activar trabajador";
                        $btn="fa fa-lock";
                    }
                    $actionBtn ='<a href="'.route("getusers",$row->crypt_id).'" class="btn btn-outline-info" title="Actualizar datos" onclick="FormPutUser(\''.$row->crypt_id.'\',this,event)"><i class="fa fa-user-edit"></i></a>';
                    $actionBtn .='&nbsp;<a href="'.route("putstatususers",$row->crypt_id).'" class="'.$class.'" title="'.$title.'" onclick="PutStatusUsers(\''.$row->crypt_id.'\',this,event)"><i class="'.$btn.'"></i></a>';
                    $actionBtn .='&nbsp;<a class="btn btn-outline-info" onclick="FormModalChangePasswd(\''.$row->crypt_id.'\',event)"><i class="fas fa-key"></i></a>';
                    return $actionBtn;
                })
                ->rawColumns(['botones','status'])
                ->make(true);
        $data1 = $data->original['data'];

        return view('usuarios::usuarios/index')->with('perfiles', $perfiles)->with('aeropuerto',$aeropuertos)->with('USUARIOS',$data1);
    }

    public function CrearUsuarios(UsuariosRequest $request){
        $pass=Str::random(10);
        //$datos = collect($request['data']);
        $this->usuario->tipo_documento=$request['tipo_documento'];
        $this->usuario->documento=$request['documento'];
        $this->usuario->p_nombre=Str::upper($request['pnombre']);
        $this->usuario->s_nombre=Str::upper($request['snombre']);
        $this->usuario->p_apellido=Str::upper($request['papellido']);
        $this->usuario->s_apellido=Str::upper($request['sapellido']);
        $this->usuario->email=$request['correo'];
        $this->usuario->username=$request['usuario'];
        $this->usuario->password=Hash::make($pass);
        $this->usuario->tlf=$request['telefono'];
        $this->usuario->perfil_id=\App\Helpers\Encryptor::decrypt($request['perfil']);
        $this->usuario->ip=$request->ip();
        $this->usuario->usuario_id=Auth::id();
        $this->usuario->save();

        $id=$this->usuario->id;
        foreach ($request->aeropuerto as $data) {
            //if($data!=null){
                $airportuser=new AeropuertosUsuarios();
                $airportuser->usuario_id=$id;
                $airportuser->aeropuerto_id=\App\Helpers\Encryptor::decrypt($data);
                $airportuser->ip=$request->ip();
                $airportuser->usuario_add_id=Auth::id();
                $airportuser->save();
            //}
        }

        $subject = "Registro de Usuarios";
        $for = $request['correo'];
        $datos['nombres'] = $request->pnombre.' '.$request->papellido;
        $datos['user'] = $request->usuario;
        $datos['pwd'] = $pass;
        $datos['id']=\App\Helpers\Encryptor::encrypt($this->usuario->id);
        //return view('usuarios::usuarios/email')->with('datos',$datos);
        Mail::send('usuarios::usuarios.email', $datos, function ($msj) use ($subject, $for) {
            $msj->subject($subject);
            $msj->to($for);
        });

        return $swal = array('status' => true, 'type' => 'success', 'title' => 'Registrado', 'message' => 'Registro realizado.');
    }

    public function ConsultarUsuario($id,Request $request){
        //dd(Browser::detect());
        $dat=$this->usuario::find(\App\Helpers\Encryptor::decrypt($id))->setAppends(['Perfil','crypt_id','Nombress']);

        if($dat->exists()){
            $listairport=AeropuertosUsuarios::where('usuario_id',$dat->id)->where('activo',true)->get();
            $array=[];
            foreach ($listairport as $data) {
                array_push($array,\App\Helpers\Encryptor::encrypt($data->aeropuerto_id));
            }
            return[0=>$dat,1=>$array];
        }
    }

    public function ActualizarUsuario(UsuariosRequest $request){
        //$datos = collect($request['data']);

        // $v = Validator::make($request->all(),
        //     [
        //         "tipo_documento"=>"bail|alpha|max:1|min:1",
        //         "documento" => "bail|required|min:5|numeric",
        //         "pnombre"=>"bail|string|required|alpha|min:3|max:50|",
        //         "snombre"=>"bail|string|required|alpha|min:3|max:50|",
        //         "papellido"=>"bail|string|required|alpha|min:3|max:50|",
        //         "sapellido"=>"bail|string|required|alpha|min:3|max:50|",
        //         "telefono"=>"bail|required|numeric|min:10",
        //         "perfil"=>"bail|required|alpha_num|string",
        //     ],
        //     [
        //         'tipo_documento.required'=>"Debe elegir el tipo de documento",
        //         "tipo_documento.min"=>"No puede introducir caracteres",
        //         "tipo_documento.max"=>"No puede introducir caracteres",
        //         'documento.required' => 'Documento es obligatorio.',
        //         'documento.min'=>'Documento longitud minima es de cinco (5) caracteres.',
        //         'documento.max'=>'Documento longitud maxima es de ocho (8) caracteres.',
        //         'documento.number'=>'Campo Documento solo admite caracteres numericos.',
        //         'pnombre.required'=>'Debe introducir su primer nombre.',
        //         'pnombre.alpha'=>'Su primer nombre no debe contener letras.',
        //         'pnombre.min'=>'Minimo de caracteres permitidos es de tres (3) para su primer nombre.',
        //         'pnombre.max'=>'Maximo de caracteres permitidos son cincuenta (50) para su primer nombre',
        //         'snombre.required'=>'Debe introducir su segundo nombre.',
        //         'snombre.alpha'=>'Su segundo nombre no debe contener letras.',
        //         'snombre.min'=>'Minimo de caracteres permitidos es de tres (3) para su segundo nombre.',
        //         'snombre.max'=>'Maximo de caracteres permitidos son cincuenta (50) para su segundo nombre',
        //         'papellido.required'=>'Debe introducir su primer apellido.',
        //         'papellido.alpha'=>'Su primer apellido no debe contener letras.',
        //         'papellido.min'=>'Minimo de caracteres permitidos es de tres (3) para su primer apellido.',
        //         'papellido.max'=>'Maximo de caracteres permitidos son cincuenta (50) para su primer apellido',
        //         'sapellido.required'=>'Debe introducir su segundo apellido.',
        //         'sapellido.alpha'=>'Su segundo apellido no debe contener letras.',
        //         'sapellido.min'=>'Minimo de caracteres permitidos es de tres (3) para su segundo apellido.',
        //         'sapellido.max'=>'Maximo de caracteres permitidos son cincuenta (50) para su segundo apellido.',
        //         'telefono.required'=>'Debe introducir su numero telefonico.',
        //         'telefono.min'=>'Telefono. Minimo de caracteres numericos es de diez (10).',
        //         'telefono.max'=>'Telefono. Maximo de caracteres numericos es de diez (11).',
        //         'telefono.numeric'=>'Telefono. Solo se Permiten numeros.',
        //         'perfil.required'=>'Debe elegir un perfil.',
        //         'perfil.alpha_num'=>'Error al elegir Perfil.',
        //     ]
        // );
        // if (!$v->fails()){//si no fallan las validaciones
            $p=$this->usuario::find(\App\Helpers\Encryptor::decrypt($request['id']));
            $p->tipo_documento=$request['tipo_documento'];
            $p->documento=$request['documento'];
            $p->p_nombre=Str::upper($request['pnombre']);
            $p->s_nombre=Str::upper($request['snombre']);
            $p->p_apellido=Str::upper($request['papellido']);
            $p->s_apellido=Str::upper($request['sapellido']);
            $p->tlf=$request['telefono'];
            $p->perfil_id=\App\Helpers\Encryptor::decrypt($request['perfil']);
            if($p->isDirty()){//compara los datos seleccionado arriba y verifica si hay cambios.
                $p->ip=$request->ip();
                $p->usuario_id=Auth::id();
                $p->fecha_registro=$this->date->toDateTimeString();
                $p->save();
                $swal = array('status' => true, 'type' => 'success', 'title' => 'Registrado', 'message' => 'Registro realizado.');
            }else{
                $swal = array('status' => true, 'type' => 'error', 'title' => 'Modificaciones', 'message' => 'No hay modificaciones a guardar.');
            }
        // }else{
        //     $swal = array('status' => false, 'type' => 'warning', 'title' => 'ERROR', 'message' => $v->errors()->first());
        // }

            $apt=new AeropuertosUsuarios();
            $apt::where('usuario_id',\App\Helpers\Encryptor::decrypt($request->id))->delete();
            for ($i=0; $i < count($request->aeropuerto); $i++) {
                $add=new AeropuertosUsuarios();
                $add->usuario_id=\App\Helpers\Encryptor::decrypt($request->id);
                $add->aeropuerto_id=\App\Helpers\Encryptor::decrypt($request->aeropuerto[$i]);
                $add->ip=$request->ip();
                $add->usuario_add_id=Auth::id();
                $add->save();
            }
        return $swal;
    }

    public function CambiarStatusUsuario($id,Request $request){
        if(!empty($id)){
            $u=$this->usuario::find(\App\Helpers\Encryptor::decrypt($id));
            if($u->activo==true){//verifica el status del usuario
                $status=false;
            }else{
                $status=true;
            }

            $this->usuario::where('id', \App\Helpers\Encryptor::decrypt($id))->update(array(
                'activo' => $status,
                'fecha_registro'=>$this->date->toDateTimeString(),
                'usuario_id'=>Auth::id())
            );

        }else{
            echo "error";
        }
    }

    public function AirportFicha(){
        $client = new Client();
        $baseUrl = env('API_ENDPOINT');
        $response = $client->get($baseUrl.'airport');
        return json_decode($response->getBody());
    }

    public function CambiarPasswdAdmin(ChangePassRequest $request){

        $this->usuario::where('id', \App\Helpers\Encryptor::decrypt($request->id))->update(array(
            'password' => Hash::make($request->passwd),
            'fecha_registro'=>$this->date->toDateTimeString(),
            'usuario_id'=>Auth::id())
        );

    }
}
