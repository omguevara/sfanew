<?php

namespace Modules\Usuarios\app\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UsuariosRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array

    {
        return match($this->method()){
            'POST'=>[
                "tipo_documento"=>"bail|alpha",
                "documento" => "bail|required|numeric",
                "pnombre"=>"bail|string|required|alpha|min:3|max:50|",
                "snombre"=>"bail|string|required|alpha|min:3|max:50|",
                "papellido"=>"bail|string|required|alpha|min:3|max:50|",
                "sapellido"=>"bail|string|nullable|alpha|min:3|max:50|",
                "telefono"=>"bail|required|min:10",
                "usuario"=>"bail|required|alpha_num|min:5|max:20|unique:\Modules\Usuarios\app\Models\Usuarios,username",
                "perfil"=>"bail|required|alpha_num|string",
                "correo"=>"bail|required|email|unique:\Modules\Usuarios\app\Models\Usuarios,email",
            ],
            'PUT'=>[
                "tipo_documento"=>"bail|alpha",
                "documento" => "bail|required|numeric",
                "pnombre"=>"bail|string|required|alpha|min:3|max:50|",
                "snombre"=>"bail|string|required|alpha|min:3|max:50|",
                "papellido"=>"bail|string|required|alpha|min:3|max:50|",
                "sapellido"=>"bail|string|nullable|alpha|min:3|max:50|",
                "telefono"=>"bail|required|min:10",
                "perfil"=>"bail|required|alpha_num|string",
            ]
        };
    }

    public function messages()
    {
        return [
            'tipo_documento.alpha'=>'<strong>TIPO DE DOCUMENTO</strong>. Solo permite letras.',
            'documento.required' => '<strong>DOCUMENTO</strong>. Debe introducir el numero de documento.',
            'documento.numeric' => '<strong>DOCUMENTO</strong>. Solo permite numeros.',

            'pnombre.required' => '<strong>PRIMER NOMBRE</strong>. Campo obligatorio.',
            'pnombre.alpha' => '<strong>PRIMER NOMBRE</strong>. Solo permite caracteres alfabéticos.',
            'pnombre.min' => '<strong>PRIMER NOMBRE</strong>. Debe tener al menos 3 caracteres.',
            'pnombre.max' => '<strong>PRIMER NOMBRE</strong>. debe tener un maximo 50 caracteres.',

            'snombre.required' => '<strong>SEGUNDO NOMBRE</strong>. Campo obligatorio.',
            'snombre.alpha' => '<strong>SEGUNDO NOMBRE</strong>. Solo permite caracteres alfabéticos.',
            'snombre.min' => '<strong>SEGUNDO NOMBRE</strong>. Debe tener al menos 3 caracteres.',
            'snombre.max' => '<strong>SEGUNDO NOMBRE</strong>. debe tener un maximo 50 caracteres.',

            'papellido.required' => '<strong>PRIMER APELLIDO</strong>. Campo obligatorio.',
            'papellido.alpha' => '<strong>PRIMER APELLIDO</strong>. Solo permite caracteres alfabéticos.',
            'papellido.min' => '<strong>PRIMER APELLIDO</strong>. Debe tener al menos 3 caracteres.',
            'papellido.max' => '<strong>PRIMER APELLIDO</strong>. debe tener un maximo 50 caracteres.',

            'sapellido.required' => '<strong>SEGUNDO APELLIDO</strong>. Campo obligatorio.',
            'sapellido.alpha' => '<strong>SEGUNDO APELLIDO</strong>. Solo permite caracteres alfabéticos.',
            'sapellido.min' => '<strong>SEGUNDO APELLIDO</strong>. Debe tener al menos 3 caracteres.',
            'sapellido.max' => '<strong>SEGUNDO APELLIDO</strong>. debe tener un maximo 50 caracteres.',

            'telefono.required' =>'<strong>TELÉFONO</strong>.  Campo obligatorio.',
            'telefono.numeric' =>'<strong>TELÉFONO</strong>. Solo permite numeros.',

            'usuario.required' =>'<strong>USUARIO</strong>.  Campo obligatorio.',
            'usuario.alpha_num' =>'<strong>USUARIO</strong>.  Solo permite caracteres alfanumérico.',
            'usuario.min' => '<strong>USUARIO</strong>. Debe tener al menos 5 caracteres.',
            'usuario.max' => '<strong>USUARIO</strong>. Debe tener un maximo 20 caracteres.',
            'usuario.unique' => '<strong>USUARIO</strong>. Ya se encuentra registrado.',

            'perfil.required' => '<strong>PERFIL</strong>. Debe seleccionar un perfil.',
            'perfil.alpha_num' => '<strong>PERFIL</strong>.  Solo permite caracteres alfanumérico.',

            'correo.required' => '<strong>CORREO</strong>. Debe seleccionar un perfil.',
            'correo.email' => '<strong>CORREO</strong>.  Formato no es válido.',
            'correo.unique' => '<strong>CORREO</strong>.   Ya se encuentra registrado.',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
        'errors' => $validator->errors(),
        'status' => true
        ], 422));
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
