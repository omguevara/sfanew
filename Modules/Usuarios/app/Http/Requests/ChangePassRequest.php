<?php

namespace Modules\Usuarios\app\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rules\Password;
class ChangePassRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array

    {
        return match($this->method()){
            'POST'=>[
                "passwd"=>"bail|alpha",
                "rpasswd" => "bail|required|numeric"
            ],
            'PUT'=>[
                "id"=>"bail|required",
                "passwd"=>["required","max:15",
                    Password::min(8)->letters()->numbers()->symbols()->mixedCase()
                ],
                "rpasswd" => "same:passwd"
            ]
        };
    }

    public function messages()
    {
        return [
            'id.required'=>'Problemas al Actualizar los datos.',
            'passwd.required'=>'<strong>Nueva Contraseña</strong>. Campo Obligatorio.',
            'passwd.max'=>'<strong>Nueva Contraseña</strong>. El número maximo de caracteres introducidos son quice(15).',

            'passwd.min' => 'NUEVA CONTRASEÑA. Debe contener solo carateres permitidos.',
            'passwd.letters' => 'NUEVA CONTRASEÑA. Debe contener al menos una letra.',
            'passwd.numbers' => 'NUEVA CONTRASEÑA. Debe contener al menos un número.',
            'passwd.symbols' => 'NUEVA CONTRASEÑA. Debe contener al menos un símbolo.',
            'passwd.mixed' => 'NUEVA CONTRASEÑA. Debe contener al menos una letra mayúscula y una minúscula.',

            'rpasswd.required' =>'<strong>Confirmacion Contraseña</strong>. Campo Obligatorio.',
            'rpasswd.same'=> 'La confirmacion de contraseña no coinciden',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
        'errors' => $validator->errors(),
        'status' => true
        ], 422));
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
