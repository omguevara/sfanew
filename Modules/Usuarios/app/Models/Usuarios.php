<?php

namespace Modules\Usuarios\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Usuarios\Database\factories\UsuariosFactory;
use App\Traits\EncryptationId;

class Usuarios extends Model
{
    use HasFactory, EncryptationId;

    protected $table = "usuarios";
    public $timestamps = false;
    protected $appends = ['crypt_id'];

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'tipo_documento',
        'documento',
        'p_nombre',
        's_nombre',
        'p_apellido',
        's_apellido',
        'verificado',
        'activo',
        'change_password',
        'email',
        'tlf',
        'username',
    ];

    protected $hidden = [
        'id',
        'perfil_id',
        'password',
        'ip',
        'usuario_id',
        'fecha_registro'
    ];

    public function getPerfilAttribute(){
        return $this->getPerfil;
    }

    public function getPerfil(){
        return $this->belongsTo(\Modules\Perfiles\app\Models\Perfiles::class, 'perfil_id');
    }

    public function getNombressAttribute(){
        return $this->p_nombre.' '.$this->s_nombre;
    }

    public function getApellidossAttribute(){
        return $this->p_apellido.' '.$this->s_apellido;
    }
    /*protected static function newFactory(): UsuariosFactory
    {
        //return UsuariosFactory::new();
    }*/
}
