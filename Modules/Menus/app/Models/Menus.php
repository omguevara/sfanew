<?php

namespace Modules\Menus\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Menus\Database\factories\MenusFactory;
use \App\Traits\EncryptationId;
#use Illuminate\Support\Facades\DB;
#use Illuminate\Support\Facades\Auth;
#use Illuminate\Support\Facades\Crypt;

class menus extends Model
{
    use HasFactory,EncryptationId;

    protected $table ='menus';
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $fillable = ['p_nombre'];
    protected $hidden = ['id'];

    protected static function newFactory(): MenusFactory
    {
        //return MenusFactory::new();
    }
}
