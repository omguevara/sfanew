<?php

namespace Modules\Aeropuertos\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Login\Database\factories\UsuariosFactory;
use \App\Traits\EncryptationId;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class AeropuertosUsuarios extends Model
{
    use HasFactory,EncryptationId;

    /**
     * The attributes that are mass assignable.
     */
    protected $table ='usuarios_aeropuertos';
    public $timestamps = false;
    protected $appends = ['crypt_id'];//,'ubicacion','oficinas'];
    protected $fillable = ['usuario_id','aeropuerto_id','usuario_add_id'];//'sede_id','oficina_id','user_id'];
    protected $hidden = ['id'];

   /* public function getAirportAttribute(){
        return $this->getAeropuertos;
    }

    public function getAeropuertos(){
        return $this->belongsTo(\Modules\Aeropuertos\app\Models\Aeropuertos::class, 'aeropuerto_id');
    }*/
    protected static function newFactory(): AeropuertosUsuariosFactory
    {
        //return AeropuertosUsuariosFactory::new();
    }
}
