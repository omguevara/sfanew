<?php

namespace Modules\Aeropuertos\app\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Login\Database\factories\UsuariosFactory;
use \App\Traits\EncryptationId;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Aeropuertos extends Model
{
    use HasFactory,EncryptationId;

    /**
     * The attributes that are mass assignable.
     */
    protected $table ='aeropuertos';
    public $timestamps = false;
    protected $appends = ['crypt_id'];//,'ubicacion','oficinas'];
    protected $fillable = ['cod_oaci','cod_iata','name_aeropuerto','administrado','clasificacion_id'];//'sede_id','oficina_id','user_id'];
    protected $hidden = ['id','fecha_registro'];

    protected static function newFactory(): AeropuertosFactory
    {
        //return AeropuertosFactory::new();
    }
}
