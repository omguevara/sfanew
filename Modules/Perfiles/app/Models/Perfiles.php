<?php

namespace Modules\Perfiles\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Perfiles\Database\factories\PerfilesFactory;
use App\Traits\EncryptationId;

class Perfiles extends Model
{
    use HasFactory, EncryptationId;

    protected $table = "perfiles";
    public $timestamps = false;
    protected $appends = ['crypt_id'];

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'descripcion',
        'ip',
        'create',
        'update',
        'delete',
        'status',
        'usuario_id',
        'activo',
        'change_password'
    ];

    protected $hidden = [
        'id',
        'fecha_registro'
    ];
    /*protected static function newFactory(): PerfilesFactory
    {
        //return PerfilesFactory::new();
    }*/
}
