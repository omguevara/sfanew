function ShowPasswd(event,id){
    $('#'+id).attr('type','text');
//    $('#'+id+'ico').remove('fa fa-solid fa-lock');
    $('.'+id+'ico').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
    $('#'+id).attr('style','background-color:rgb(135,170,222)');
    setTimeout(() => {
        $('.'+id+'ico').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        $('#'+id).attr('type','password');
        $('#'+id).attr('style','background-color:rgb(255,255,255)');
      }, 5000);
}


function SendResetPasswd(event){
    event.preventDefault();
    if($('#FormResetPasswd').valid()){
        $('#FormResetPasswd').submit();
    }
}
