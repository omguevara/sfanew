function AlphaNumeric(event){
    var code = (event.which) ? event.which : event.keyCode;
    if((code >= 48 && code <= 57) || (code >= 65 && code <= 90) || (code >= 97 && code <= 122) ) {
      return true;
    }else{
        return false;
    }
}
/*
function Number(event){
    var code = (event.which) ? event.which : event.keyCode;
    if(code >= 48 && code <= 57) {
      return true;
    }else{
        return false;
    }
}*/

function Alpha(event){
    var code = (event.which) ? event.which : event.keyCode;
    if((code >= 65 && code <= 90) || (code >= 97 && code <= 122) ) {
      return true;
    }else{
        return false;
    }
}

function CloseModal(id,event){
    event.preventDefault()
    $('#'+id+' form')[0].reset();
    $('#'+id).modal('hide');
}
