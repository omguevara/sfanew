function FormPutUser(id,obj,event){
    event.preventDefault();
    $.get(obj.href,id).done(function(resp){
        //$('#StatusPut').val((resp.bloqueado==true? 0: 1));
        $('#FormPutUser #tipo_documento').val((resp[0].tipo_documento=='V'? 'V': 'E'));
        $('#FormPutUser #documento').val(resp[0].documento);
        $('#FormPutUser #id').val(resp[0].crypt_id);
        $('#FormPutUser #pnombre').val(resp[0].p_nombre);
        $('#FormPutUser #snombre').val(resp[0].s_nombre);
        $('#FormPutUser #papellido').val(resp[0].p_apellido);
        $('#FormPutUser #sapellido').val(resp[0].s_apellido);
        $('#FormPutUser #usuario').val(resp[0].username);
        $('#FormPutUser #correo').val(resp[0].email);
        $('#FormPutUser #telefono').val(resp[0].tlf);
        $('#FormPutUser #perfil').val(resp[0].Perfil.crypt_id);
        $('#ModalUsuariosUpdate').modal('show');
        /*var t=Object.keys(resp[1]);
        for (let index = 0; index < t.length; index++) {
            $("#FormPutUser #aeropuerto").val(t[index]);
        }
        $("#FormPutUser #aeropuerto").trigger('change');*/
        $(".select_chosen").val(resp[1]).trigger("chosen:updated");
    }).fail(function(ru){
            if(ru.status==401){
                SessLost();
            }else{
                Message('error','Error inesperado','Problemas al realizar la petición.');
            }
    });
}

function ButtPutUser(data,event){
    event.preventDefault();
    if($("#FormPutUser").valid()){
        Swal.fire({
            title: `Actualizar`,
            text: "Esta seguro de Modificar los datos de este usuario",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: "Si, Guardar",
            cancelButtonText: "Cancelar",
        })
        .then(resultado => {
            if (resultado.value) {// Hicieron click en "Sí"
                $.post($('#FormPutUser').attr('action'),data).done(function(resp){
                    Message(resp.type,resp.title,resp.message);
                }).fail(function(ru){
                    console.log("error");
                    var t=error.responseJSON.errors;
                    var i=0;
                    for( key in t){
                        $('body').prepend(`
                            <div class="alert alert-danger alert-dismissible fade show" style="position: absolute;right:0;top:`+i+`px;z-index:9999;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                ${t[key][0]}.
                            </div>
                        `);
                        i+=70;
                    }
                    $('.toast').toast('show');
                   /* if(ru.status==401){
                        SessLost();
                    }else{
                        Message('error','Error inesperado','Problemas al realizar la petición.');
                    }*/
                });
            } else {// Dijeron que no

            }
        });
    }

}

function PutStatusUsers(id,obj,event){
    event.preventDefault();
    Swal.fire({
        title: `Actualizar`,
        text: "Esta seguro de INACTIVAR este usuario",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Si, INACTIVAR",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {// Hicieron click en "Sí"
            $.get(obj.href,id).done(function(resp){
                location.reload();
            }).fail(function(ru){
                    if(ru.status==401){
                        SessLost();
                    }else{
                        Message('error','Error inesperado','Problemas al realizar la petición.');
                    }
            });
        } else {// Dijeron que no

        }
    });


}

function FormModalChangePasswd(id,event){
    event.preventDefault();
    $('#ModalChangePassword').modal('show');
    $('#FormPutUser #id').val(id);
}
