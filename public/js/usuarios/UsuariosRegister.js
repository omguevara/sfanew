$(document).ready(function() {
    $('#RegistrarUsuario').on('click',function(){
        event.preventDefault();
        if($("#FormAddUser").valid()){
            Swal.fire({
                title: `Registrar`,
                text: "Esta seguro de realizar este registro",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Si, Registrar",
                cancelButtonText: "Cancelar",
            })
            .then(resultado => {
                if (resultado.value) {// Hicieron click en "Sí"
                    $.post($('#FormAddUser').attr('action'),
                        $('#FormAddUser').serializeObject()
                    )
                        .done(function(resp){
                            Message(resp.type,resp.title,resp.message);
                    }).fail(function(error){
                        var t=error.responseJSON.errors;
                        var i=0;
                        for( key in t){
                            $('body').prepend(`
                                <div class="alert alert-danger alert-dismissible fade show" style="position: absolute;right:0;top:`+i+`px;z-index:9999;">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    ${t[key][0]}.
                                </div>
                            `);
                            i+=70;
                        }
                        $('.toast').toast('show');
                });
                } else {// Dijeron que no

                }
            });
        }
    });
});

