<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class MenuProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        $list=\Modules\Menus\app\Models\Menus::where('status',true)->orderBy('id')->get();
        $menu=array();


        foreach ($list as $value) {
            array_push(
                $menu,
                array('text' => $value->name_menu,'url'  => $value->ruta,'icon' => $value->icono),
            );
        }

        \Event::listen('JeroenNoten\LaravelAdminLte\Events\BuildingMenu', function ($event) use($menu) {
            foreach($menu as $value){
                $event->menu->addBefore('salir',$value);
            }
        });

        return $next($request);
    }
}
